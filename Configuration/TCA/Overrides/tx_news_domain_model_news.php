<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}
$ll = 'LLL:EXT:news/Resources/Private/Language/locallang_db.xlf:';

use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

if (ExtensionManagementUtility::isLoaded('news')) {

    // icon-identifier for new type:
    $GLOBALS['TCA']['tx_news_domain_model_news']['ctrl']['typeicon_classes']['boto_newsevents'] = 'ext-news-type-boto_newsevents';

    // register new type for news-recors, also add the icon-identifier:
    $GLOBALS['TCA']['tx_news_domain_model_news']['columns']['type']['config']['items']['boto_newsevents'] =
        ['LLL:EXT:boto_newsevents/Resources/Private/Language/locallang_be.xlf:mod.tx_news_domain_model_news.type.event', 'boto_newsevents', 'ext-news-type-boto_newsevents'] ;


    // add new field for gallery-display:
    $temporaryColumns = [
        'boto_eventnews_event_end' => [
            'exclude' => false,
            'label' => 'LLL:EXT:boto_newsevents/Resources/Private/Language/locallang_be.xlf:txnews.boto_eventnewsevent_end',
            'config' => [
                'type' => 'input',
                'size' => 16,
                'eval' => 'datetime' . ($configuration->getDateTimeRequired() ? ',required' : ''),
            ]
        ],
        'boto_eventnews_full_day' => [
            'exclude' => 1,
            'l10n_mode' => 'exclude',
            'label'   => 'LLL:EXT:boto_newsevents/Resources/Private/Language/locallang_be.xlf:txnews.boto_eventnews_full_day',
            'config'  => [
                'type'    => 'check',
                'default' => 0,
            ],
        ],
        'boto_eventnews_organizer' => [
            'exclude' => true,
            'l10n_mode' => 'noCopy',
            'label'   => 'LLL:EXT:boto_newsevents/Resources/Private/Language/locallang_be.xlf:txnews.boto_eventnews_organizer',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ]
        ],
        'boto_eventnews_organizer_link' => [
            'exclude' => true,
            'l10n_mode' => 'noCopy',
            'label'   => 'LLL:EXT:boto_newsevents/Resources/Private/Language/locallang_be.xlf:txnews.boto_eventnews_organizer_link',
            'config' => [
                'type' => 'input',
                'size' => 50,
                'eval' => 'trim',
                'wizards' => [
                    'link' => [
                        'type' => 'popup',
                        'title' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_link_formlabel',
                        'icon' => 'actions-wizard-link',
                        'module' => [
                            'name' => 'wizard_link',
                        ],
                        'JSopenParams' => 'height=600,width=800,status=0,menubar=0,scrollbars=1'
                    ]
                ],
            ]
        ],
        'boto_eventnews_location' => [
            'exclude' => true,
            'l10n_mode' => 'noCopy',
            'label'   => 'LLL:EXT:boto_newsevents/Resources/Private/Language/locallang_be.xlf:txnews.boto_eventnews_location',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ]
        ],
        'boto_eventnews_location_link' => [
            'exclude' => true,
            'l10n_mode' => 'noCopy',
            'label'   => 'LLL:EXT:boto_newsevents/Resources/Private/Language/locallang_be.xlf:txnews.boto_eventnews_location_link',
            'config' => [
                'type' => 'input',
                'size' => 50,
                'eval' => 'trim',
                'wizards' => [
                    'link' => [
                        'type' => 'popup',
                        'title' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_link_formlabel',
                        'icon' => 'actions-wizard-link',
                        'module' => [
                            'name' => 'wizard_link',
                        ],
                        'JSopenParams' => 'height=600,width=800,status=0,menubar=0,scrollbars=1'
                    ]
                ],
            ]
        ],
    ];

    ExtensionManagementUtility::addTCAcolumns(
        'tx_news_domain_model_news',
        $temporaryColumns
    );

    // control fields only to above registered news types
    // basically it uses some fields from the default news type.
    $GLOBALS['TCA']['tx_news_domain_model_news']['types']['boto_newsevents'] = [
        'columnsOverrides' => [
            'bodytext' => [
                'defaultExtras' => 'richtext:rte_transform[mode=ts_css]'
            ],
        ],
        'showitem' => 'l10n_parent, l10n_diffsource,
					title,--palette--;;palette_tx_botonewseventsCore,teaser,
					--palette--;;paletteDate,boto_eventnews_event_end,boto_eventnews_full_day,
					--palette--;;palette_tx_botonewseventsOrganizer,
					--palette--;;palette_tx_botonewseventsLocation,
					bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:rte_enabled_formlabel,

				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;paletteAccess,

				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.options,categories,tags,
				--div--;' . $ll . 'tx_news_domain_model_news.tabs.relations,fal_media,fal_related_files,related_links,related,related_from,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.metadata,
					--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.palettes.metatags;metatags,
					--palette--;' . $ll . 'tx_news_domain_model_news.palettes.alternativeTitles;alternativeTitles,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:pages.tabs.extended,'
    ];
    // Add the new palette:
    $GLOBALS['TCA']['tx_news_domain_model_news']['palettes']['palette_tx_botonewseventsCore'] = array(
        'showitem' => 'type, sys_language_uid, hidden,'
    );
    $GLOBALS['TCA']['tx_news_domain_model_news']['palettes']['palette_tx_botonewseventsOrganizer'] = array(
        'showitem' => 'boto_eventnews_organizer,--linebreak--,boto_eventnews_organizer_link,'
    );
    $GLOBALS['TCA']['tx_news_domain_model_news']['palettes']['palette_tx_botonewseventsLocation'] = array(
        'showitem' => 'boto_eventnews_location,--linebreak--,boto_eventnews_location_link,'
    );

}
