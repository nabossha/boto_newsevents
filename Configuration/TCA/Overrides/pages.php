<?php
/**
 * Created by PhpStorm.
 * User: nabossha
 * Date: 23.06.2016
 * Time: 11:27
 */

defined('TYPO3_MODE') or exit();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'boto_newsevents',
    '/Configuration/TSconfig/Page/TCAdefaults.ts',
    'TCADefaults: news-Type=Event');
