<?php

$EM_CONF[$_EXTKEY] = array(
    'title'              => 'BossharTong: News-Events',
    'description'        => 'extends tx_news with an article type for events',
    'category'           => 'plugin',
    'version'            => '1.0.7',
    'shy'                => '',
    'priority'           => '',
    'loadOrder'          => '',
    'module'             => '',
    'state'              => 'stable',
    'uploadfolder'       => 0,
    'createDirs'         => '',
    'modify_tables'      => '',
    'clearcacheonload'   => 1,
    'lockType'           => '',
    'author'             => 'Nando Bosshart',
    'author_email'       => 'typo3@bosshartong.ch',
    'author_company'     => 'BossharTong GmbH',
    'CGLcompliance'      => null,
    'CGLcompliance_note' => null,
    'constraints'        => array(
        'depends' => array(
            'typo3' => '7.6.0-8.7.99',
            'news' => '5.0',
        ),
        'conflicts' => array(
            'eventnews' => '',
        ),
        'suggests' => array(
        ),
    ),
    'autoload' => array(
        'psr-4' => array(
            'Bosshartong\\BotoNewsevents\\' => 'Classes',
        ),
    ),
);
