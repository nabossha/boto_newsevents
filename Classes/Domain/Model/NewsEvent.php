<?php
namespace Bosshartong\BotoNewsevents\Domain\Model;


/**
 * News model
 *
 * @package TYPO3
 * @subpackage tx_news
 */
class NewsEvent extends \GeorgRinger\News\Domain\Model\News {

    /**
     * @var boolean
     */
    protected $botoEventnewsFullDay;

    /**
     * @var \DateTime
     */
    protected $botoEventnewsEventEnd;

    /**
     * @var string
     */
    protected $botoEventnewsOrganizer;

    /**
     * @var string
     */
    protected $botoEventnewsOrganizerLink;


    /**
     * @var string
     */
    protected $botoEventnewsLocation;

    /**
     * @var string
     */
    protected $botoEventnewsLocationLink;

    /**
     * Set $botoEventnewsFullDay flag
     *
     * @param bool $botoEventnewsFullDay flag
     */
    public function setBotoEventnewsFullDay($botoEventnewsFullDay) {
        $this->botoEventnewsFullDay = $botoEventnewsFullDay;
    }

    /**
     * Get $botoEventnewsFullDay flag
     *
     * @return bool
     */
    public function getBotoEventnewsFullDay() {
        return $this->botoEventnewsFullDay;
    }


    /**
     * Get botoEventnewsEventEnd date
     *
     * @return \DateTime
     */
    public function getBotoEventnewsEventEnd()
    {
        return $this->botoEventnewsEventEnd;
    }

    /**
     * Set botoEventnewsEventEnd date
     *
     * @param \DateTime $botoEventnewsEventEnd date
     */
    public function setBotoEventnewsEventEnd($botoEventnewsEventEnd)
    {
        $this->botoEventnewsEventEnd = $botoEventnewsEventEnd;
    }

    /**
     * Get Organizer
     *
     * @return string
     */
    public function getBotoEventnewsOrganizer()
    {
        return $this->botoEventnewsOrganizer;
    }

    /**
     * Set Organizer
     *
     * @param string $botoEventnewsOrganizer
     */
    public function setBotoEventnewsOrganizer($botoEventnewsOrganizer)
    {
        $this->botoEventnewsOrganizer = $botoEventnewsOrganizer;
    }

    /**
     * Get OrganizerLink
     *
     * @return string
     */
    public function getBotoEventnewsOrganizerLink()
    {
        return $this->botoEventnewsOrganizerLink;
    }

    /**
     * Set OrganizerLink
     *
     * @param string $botoEventnewsOrganizerLink
     */
    public function setBotoEventnewsOrganizerLink($botoEventnewsOrganizerLink)
    {
        $this->botoEventnewsOrganizerLink = $botoEventnewsOrganizerLink;
    }


    /**
     * Get Location
     *
     * @return string
     */
    public function getBotoEventnewsLocation()
    {
        return $this->botoEventnewsLocation;
    }

    /**
     * Set Location
     *
     * @param string $botoEventnewsLocation
     */
    public function setBotoEventnewsLocation($botoEventnewsLocation)
    {
        $this->botoEventnewsLocation = $botoEventnewsLocation;
    }



    /**
     * Get LocationLink
     *
     * @return string
     */
    public function getBotoEventnewsLocationLink()
    {
        return $this->botoEventnewsLocationLink;
    }

    /**
     * Set LocationLink
     *
     * @param string $botoEventnewsLocationLink
     */
    public function setBotoEventnewsLocationLink($botoEventnewsLocationLink)
    {
        $this->botoEventnewsLocationLink = $botoEventnewsLocationLink;
    }
}