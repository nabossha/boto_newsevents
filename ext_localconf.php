<?php
defined('TYPO3_MODE') or die();
$boot = function () {
    // register icons:
    if (TYPO3_MODE === 'BE') {
        $icons        = [
            'ext-news-type-boto_newsevents' => 'news_domain_model_news_event.svg'
        ];
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        foreach ($icons as $identifier => $path) {
            $iconRegistry->registerIcon(
                $identifier,
                \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                ['source' => 'EXT:boto_newsevents/Resources/Public/Icons/' . $path]
            );
        }
    }
};
$boot();
unset($boot);
