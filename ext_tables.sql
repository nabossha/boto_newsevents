#
# Table structure for table 'tx_news_domain_model_news'
#

CREATE TABLE tx_news_domain_model_news (
	boto_eventnews_full_day tinyint(1) unsigned DEFAULT '0' NOT NULL,
	boto_eventnews_event_end int(11) unsigned DEFAULT '0',
	boto_eventnews_organizer varchar(255) DEFAULT '' NOT NULL,
	boto_eventnews_organizer_link varchar(255) DEFAULT '' NOT NULL,
	boto_eventnews_location varchar(255) DEFAULT '' NOT NULL,
	boto_eventnews_location_link varchar(255) DEFAULT '' NOT NULL,
);
